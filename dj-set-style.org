#+NAME: Dat DJ Set Style
#+AUTHOR: Zach Mandeville
#+TODO: DREAM MANIFESTING | REALITY

* Introduction
  This is the basic README.  I want it to not be intimidating, so I think the best is to break up the longer stuff into other folders.  This just says, 'cool make it yr own, and check out distro next.'
  #+BEGIN_SRC markdown :tangle ./README.md :noweb yes
    # Welcome to the 'DAT DJ SET STYLE' Zine Template

    You are visiting an (incredible) zine that's been made using the dat dj set style template.  This means that you can copy this template and use it as the start of your own (likely also incredible** zine.

    ## Is this You? Do you wanna make yr own zine kinda like this one?

    SWEET! This readme will guide you through how to do this.  But here's a super short version:
    - You'll make a copy of this code with a literal click of the button.
    - You can  quickly make it yr own by switching out an mp3 and adjusting a text file.
    - On top of this, you can adjust the colors and CSS to make it more your own.
    - You can even just change all the code to make it fully your own.  It's all yours (as long as you share it just as freely after**.

    There's more details on how to do each thing within the folders you see up above.

    ,**The First Folder you wanna check out is Distro***
    After that, check out Aesthetic.
    Finally, check out javascripts.

    # How to Edit and Customize the Zine

    The majority of changes can be done entirely within beaker using this here files page (though you can use your local copy and yr favorite text editor too).  Up above is all the folders and files that make up the zine.  click on any file to see its contents.  Then, click the edit pencil in the top right of the text window to edit it as you please.

    If you want to adjust the javascript, it's made using [node](https://nodejs.org/en/) and [choo](https://choo.io).  Choo has a great tutorial on their page for getting started with this kinda development.

    # Thank Yous
    - Thanks to Beaker Browser and its team for this wonderful place.
    - Thanks to Kawaiipunk and [SSB](https://ssb.nz), for sharing the [original set](https://soundcloud.com/laipower/live-set-fast-forward-16-8-18) that inspired this here thing.
    - Thanks to [Raphael Bastide](https://raphaelbastide.com/) for the delightful code with the ssb hash worms.
    - Thanks to choo for the cute and accessible framework for building this.
    - Thanks to [smarkt](https://github.com/jondashkyle/smarkt), by jon-kyle, for giving a great way on building out the text files.
    - Thanks to Font Awesome for the icons used for download, play, pause, and info.  All icons used under the Creative Commons License, which can be read [here](https://fontawesome.com/license)
    - Thanks to the Scuttlebutt commuity for making cool shit all the time.
    - Thanks to you for reading this, and making all of this matter!

    <<Soundtrack>>

    ## Made on a couch and at a kitchen table while...

    Spring starts up here in New Zealand, moving back with some friends on their beautiful airship home up in Hataitai, an empty kitchen table and a beautiful view of laundry whipping in the wind on the porch outside and the beautiful ocean just beyond. Taking breaks to eat Angelica's oat bars and go through a kundalini kriya with her.

  #+END_SRC
* Intentions
** REALITY See what it is that I'm talking about
   CLOSED: [2018-12-02 Sun 19:20]
** REALITY Set up a rhythm.
   CLOSED: [2018-12-02 Sun 19:20]
** MANIFESTING be able to download the song from their audio zine
** DREAM clean up state names so it's all lowercase and clear.
** DREAM be able to have the background be their full album cover.
** DREAM is to be able to see the current track progress all visually.
** DREAM Check to make sure contents of music folder is actually music.
* How it Works 
  A cool musician will have a hot track they want to share.  So they'll make a copy of this zine, and then make two crucial changes to it:
- They'll throw in their own music
- They'll change the info.txt file in the distro with info about their music!
* aesthetic
** Colors and Fonts 
   #+NAME: Colors and Fonts 
   #+BEGIN_SRC css :tangle ./aesthetic/colors-and-fonts.css :noweb yes
     /*
       CSS Magic!
       You can set the colors and images for the main part of your zine here.  If you need to do even more customization, check out the main.css page.
     ,*/

     :root {
         /* For Background, the center / cover determines how we lay out the image on the page.
         For a new image, put whatever you want for the background in yr aesthetic folder and then
         reference the file name here as ./filename.jpg_or_what_have_you.
         You can find out more about the background property here: https://developer.mozilla.org/en-US/docs/Web/CSS/background */
         --background: center / contain no-repeat url('./lai-power-cover.jpg') #1A1627;

         /* Opening deals with the track title that shows when the page is first loaded. */
         --opening_font: monospace;
         --opening_color: pink;
         --opening_bg: transparent;

         /* we use an rgba for our playerbutton so we can get it nicely translucent.  you can have it be a solid color too,
            if you want. There's a good guide to color here: https://developer.mozilla.org/en-US/docs/Web/HTML/Applying_color  */
         --playerButton: rgba(112, 111, 181, 0.13);

         /* This is for our time stamp at the bottom of the page. */
         --time_bg: transparent;
         --time_color: pink;
         --time_font: monospace;

         /* Our Download Link.  We hare having the icon be an svg, but you can replace it with a different svg if you'd like. */

         <<Download Link Colors>>
         <<Info Icons Colors>>

         /* This is for our Question Mark, that triggers the info about the track. */
         --q_bg: rgba(112, 111, 181, 0.13);
         --q_color: pink;

         /* Now for the InfoBox, that comes up when you hit the Question Mark */
         --info_bg: transparent;
         --info_color: pink;
         --info_link: gold;
         --info_font: monospace;

         /* hashworm.  This hides our ssb key in the cutest way in the info box.  
            Endless thanks to raphael batiste for this code. */
         --worm_bg: tranparent;
         --worm_color: lightgreen;
     }

   #+END_SRC
** Main
   #+NAME: Main Aesthetic
   #+BEGIN_SRC css :tangle ./aesthetic/main.css
     /*
         ,* Want to change this website's styes? Learn more about CSS:
         ,* https://developer.mozilla.org/en-US/docs/Learn/CSS/Introduction_to_CSS
         ,*/

        body, html {
            padding: 0;
            margin: 0;
            padding: 0 20px;
            background: var(--background);
            height: 100vh;
            font-size: 20px;
            font-family: serif;
            color: white;
        }

        #openingInfo {
          height: 100vh;
          max-width: 80%;
          margin: auto;
          padding-top: 0;
          padding-left: 2em;
          position: fixed;
          top: 0;
          left: 0;
          display: flex;
          flex-direction: column;
          justify-content: center;
          font-family: var(--opening_font);
          color: var(--opening_color);
          background: var(--opening_bg);
          animation: fadeOut 10s forwards;
        }
        @keyframes fadeOut {
            from {
                opacity: 1;
            }
            to {
                opacity: 0;
            }
        }

        #openingInfo h1 {
          margin-bottom: 0;
        }

        #openingInfo p {
          margin-top: 0;
          font-size: 2em;
        }

        #time {
            font-family: var(--time_font);
            background: var(--time_bg);
            color: var(--time_color);
            font-size: 2em;
            text-align: center;
        }

        .wrapper{
            height: 100%;
            width: 100%;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
        }

        .wrapper h1{
            font-size: 4.5em;
        }
        @media (max-width: 667px){
            .wrapper h1{
                font-size: 3em;
            }
        }

        .wrapper a{
            text-align: center;
            text-decoration: none;
            color: black;

        }

        .wrapper a:hover{
            text-decoration: underline;

        }

        #player-button {
            height: 80vh;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;

        }

        #player-button svg{
            cursor: pointer;
            height: 80%;
            fill: var(--playerButton); 
            z-index: 1000;

        }
        @media (max-width: 667px) {
            #player-button svg{
                height: 60%;

            }

        }

        #infoBox{
            position: absolute;
            top: 0;
            left: 0;
            height: 85vh;
            overflow-y: scroll;
            max-width: 100vw;
            padding: 1em;
            display: flex;
            flex-direction: column;
            justify-content: flex-start;
            font-family: var(--info_font);
            color: var(--info_color);
            background: var(--info_bg);
        }

        #infoBox a{
          color: var(--info_link);
     }

        #set-name {
            margin-bottom: 0;
        }

        #byline {
            margin-top: 0;
        }

        #SSB {
            font-size: 1.2em;
            margin-bottom: 0;
        }

        #SSB + p {
            margin-top: 0;
        }

        .instruction {
            font-size: 0.65em;
        }
        #additionalInfo {
            font-size: 0.8em;
        }
        @media(min-width: 667px) {

            #infoBox{
                font-size: 2em;
            }

            #set-name {
                margin-bottom: 0;
            }

            #byline {
                margin-top: 0;
            }

            #SSB {
                font-size: 1.2em;
                margin-bottom: 0;
            }

            #SSB + p {
                margin-top: 0;
            }

            .instruction {
                font-size: 0.65em;
            }
            #additionalInfo{
                font-size: 0.65em;
            }
        }

        /*This following code for the hashworm comes from Raphael Bastide, and it makes me soooo happy.  
                You can find the original site here: dat://ff176b8c453939142d592e1c2c5eef7c9c69423e1f85bc2560af52e6a8750ab2/ */
        code{
            padding:5px;
            display:inline-block;
        }
        i{
            font-style:normal;
        }
        code.wormed i{
            background: var(--worm_bg);
            color: var(--worm_color);
            letter-spacing:-.54em;
            font-weight: bold;
            margin-right:.5em;
            transition:all .2s ease;
            cursor:help;
            /*text-shadow: 1px 0px 1px black;*/
            position: relative;
        }
        code.wormed i:after{
            content: "";
            width: .45em;
            height: .45em;
            border-radius: .4em;
            position: absolute;
            top: 0.0em;
            right: -.6em;
            background: white;
            border: .2em solid black;
        }
        code.wormed i:before{
            content: "";
            width: .25em;
            height: .25em;
            border-radius: .3em;
            position: absolute;
            top: .40em;
            right: -.40em;
            z-index: 3;
            background: black;
        }
        code.wormed i em{
            margin-right:5px;
            transition:all .3s ease-in;
        }
        code.wormed.hovermode i:hover{
            display:inline-block;
            transform:scale(2);
            padding: 20px;
            transform-origin:center;
            background:#fcfcfc;
            box-shadow:0px 0px 2px #ccc;
            text-shadow: none;
        }

        code.wormed.hovermode i:hover .c1,
        code.wormed.hovermode i:hover .c2,
        code.wormed.hovermode i:hover .c3,
        code.wormed.hovermode i:hover .c4{
            display:inline-block;
            letter-spacing:normal;
            transform-origin:center;
        }

   #+END_SRC
   
* Javascripts
** Views
*** The Download Button
    :PROPERTIES:
    :header-args: :tangle ./javascripts/views/download-link.js
    :END:
 We want this to be an icon that, when clicked, downloads the file included in the zine.     It should appear in the top right of the file, instead of the question mark.

    Downloading a link is mad simple these days!  You just need to include a download attribute in yr a link, with the value of it being the file name we going to download.  If you don't include the value, you get the shifty-as text 'download download.mp3?' 
    So we'll have the href and the download just be the audio file as set in state.  
   
    #+NAME: Download Link
    #+BEGIN_SRC javascript
      var html = require('choo/html')
      var raw = require('nanohtml/raw')

      module.exports = (state, emit) => {
      var audio = state.set.Audio

        return html`
          <div id='download-link'>
          <a href='distro/music/${audio}' title='download the file ${audio}' download=${audio}>${downloadIcon()}</a>
          </div>
        `
        function downloadIcon () {
          return html`
          <svg aria-hidden="true" data-prefix="far" data-icon="arrow-alt-circle-down" class="svg-inline--fa fa-arrow-alt-circle-down fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm0 448c-110.5 0-200-89.5-200-200S145.5 56 256 56s200 89.5 200 200-89.5 200-200 200zm-32-316v116h-67c-10.7 0-16 12.9-8.5 20.5l99 99c4.7 4.7 12.3 4.7 17 0l99-99c7.6-7.6 2.2-20.5-8.5-20.5h-67V140c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12z"></path></svg>
          `
        }
      }
    #+END_SRC
     
    We can also set our aesthetic here.
    
    #+NAME: Download Link Aesthetic
    #+BEGIN_SRC css :tangle ./aesthetic/main.css
      /*
         The Download Link Button
      ,*/

      #download-link{
          position: absolute;
          background: var(--download-button_icon);
          top: 0;
          right: 0;
          margin: 1em;
      }

      #download-link svg{
          color: var(--download-link_color);
          width: var(--download-link_size);
      }
    #+END_SRC
    
    and the colors used.
    
    #+NAME: Download Link Colors
    #+BEGIN_SRC css :tangle no :noweb yes
      --download-link_color: white;
      --download-link_size: 2em;
    #+END_SRC

*** Main View
    :PROPERTIES:
    :header-args: :tangle ./javascripts/views/main.js
    :END:
    This is the overall single page, with everything else little sub-components within it 
**** The Code
    #+NAME: main.js
    #+BEGIN_SRC javascript
      const html = require('nanohtml')

      const PlayerButton = require('./playerButton.js')
      const InfoBox = require('./info-box')
      const OpeningInfo = require('./opening-info')
      const DownloadLink = require('./download-link')

      var player = new PlayerButton()

      module.exports = (state, emit) => {
        if (state.set) {
          return html`
            <body id='meditation'>
            <div class='wrapper'>
                ${OpeningInfo(state, emit)}
                ${DownloadLink(state, emit)}
                ${player.render(state, emit)}
                ${InfoBox(state, emit)}
                <div id='info-icons' onclick=${() => emit('toggle infoBox')}>
                  ${infoToggle(state)}
                </div>
              </div>
          </body>
          `
        }

        function infoToggle (state) {
          if (!state.infoBoxVisible) {
            return questionIcon()
          } else {
            return xIcon()
          }
        }

        function questionIcon () {
          return html`
      <svg id='question-icon' aria-hidden="true" data-prefix="far" data-icon="question-circle" class="svg-inline--fa fa-question-circle fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M256 8C119.043 8 8 119.083 8 256c0 136.997 111.043 248 248 248s248-111.003 248-248C504 119.083 392.957 8 256 8zm0 448c-110.532 0-200-89.431-200-200 0-110.495 89.472-200 200-200 110.491 0 200 89.471 200 200 0 110.53-89.431 200-200 200zm107.244-255.2c0 67.052-72.421 68.084-72.421 92.863V300c0 6.627-5.373 12-12 12h-45.647c-6.627 0-12-5.373-12-12v-8.659c0-35.745 27.1-50.034 47.579-61.516 17.561-9.845 28.324-16.541 28.324-29.579 0-17.246-21.999-28.693-39.784-28.693-23.189 0-33.894 10.977-48.942 29.969-4.057 5.12-11.46 6.071-16.666 2.124l-27.824-21.098c-5.107-3.872-6.251-11.066-2.644-16.363C184.846 131.491 214.94 112 261.794 112c49.071 0 101.45 38.304 101.45 88.8zM298 368c0 23.159-18.841 42-42 42s-42-18.841-42-42 18.841-42 42-42 42 18.841 42 42z"></path></svg>
      `
        }

        function xIcon () {
          return html`
            <svg id='x-icon' aria-hidden="true" data-prefix="far" data-icon="times-circle" class="svg-inline--fa fa-times-circle fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm0 448c-110.5 0-200-89.5-200-200S145.5 56 256 56s200 89.5 200 200-89.5 200-200 200zm101.8-262.2L295.6 256l62.2 62.2c4.7 4.7 4.7 12.3 0 17l-22.6 22.6c-4.7 4.7-12.3 4.7-17 0L256 295.6l-62.2 62.2c-4.7 4.7-12.3 4.7-17 0l-22.6-22.6c-4.7-4.7-4.7-12.3 0-17l62.2-62.2-62.2-62.2c-4.7-4.7-4.7-12.3 0-17l22.6-22.6c4.7-4.7 12.3-4.7 17 0l62.2 62.2 62.2-62.2c4.7-4.7 12.3-4.7 17 0l22.6 22.6c4.7 4.7 4.7 12.3 0 17z"></path></svg>
            `
        }
      }
    #+END_SRC
**** aesthetic
     #+NAME: Main Aesthetic
     #+BEGIN_SRC css :tangle ./aesthetic/main.css
       /* Info Icons */
       #info-icons{
           position: absolute;
           bottom: 0;
           right: 0;
           margin: 2em;
       }

       #question-icon {
           width: var(--info-icon_size);
           color: var(--info-icon_color);
       }
       #x-icon {
           width: var(--x-info-icon_size);
           color: var(--x-info-icon_color);
       }

     #+END_SRC
     
     #+NAME: Main Colors and Fonts
     #+BEGIN_SRC css :tangle ./aesthetic/main.css
       /* info icons */

     #+END_SRC
     
     #+NAME: Info Icons Colors
     #+BEGIN_SRC css :noweb yes :tangle no
     /* The Icons to toggle our info on or off */
       --info-icon_color: white;
       --info-icon_size: 2em;
       --x-info-icon_size: 2em;
       --x-info-icon_color: white;
     #+END_SRC

*** Opening Info
    :PROPERTIES:
    :header-args: :tangle ./javascripts/views/opening-info.js
    :END:
    The title track that appears for a moment and then fades away.
    #+NAME: opening-info.js
    #+BEGIN_SRC js
      const html = require('nanohtml')

      module.exports = (state, emit) => {
        if (state.set.name && state.openingVisible) {
          return html`
          <div id='openingInfo'>
          <h1>${state.set.name}</h1>
          <p>${state.set.DJ}</p>
          </div>
        `
        }
      }

    #+END_SRC
    
    
*** InfoBox
    :PROPERTIES:
    :header-args: :tangle ./javascripts/views/info-box.js
    :END:
    
    #+NAME: info-box.js
    #+BEGIN_SRC js const html = require('nanohtml')
      const html = require('choo/html')
      const md = require('markdown-it')()
      const raw = require('nanohtml/raw')

      module.exports = (state, emit) => {
        console.log(state.infoBoxVisible)
        if (state.set && state.infoBoxVisible) {
          var set = state.set
          return html`
           <div id='infoBox'>
             <h2 id='set-name'>'${set.name}'</h2>
             <p id='byline'>by ${set.DJ}</p>
             ${renderAdditionalInfo()}
             <h3 id='SSB'>Find them on SSB:</h3>
             <p> ${set.SSB.name} <code id='ssb-key' class='wormed'><i title=${set.SSB.key}>${set.SSB.key}</i></code></p>
             <p class='instruction'>hover over the worm to see their key, highlight entire worm and press cmd+c to copy to your clipboard.</p>
           </div>
       `
        }
        function renderAdditionalInfo () {
          if (state.set.Additional) {
            var additional = raw(md.render(state.set.Additional))
            return html`
            <div id='additionalInfo'>
              ${additional}
            </div>
          `
          }
        }
      }


    #+END_SRC

* Soundtrack
  #+NAME: Soundtrack
  #+BEGIN_SRC markdown :noweb yes :tangle no
    # Soundtrack
    During creation I listened to:
      - a lot of Whitney Ballen's album, [You're a Shooting Star, I'm A Sinking Ship](https://whitneyballen.bandcamp.com/)
      - the lai power set linked above.
      - Four Tet's [New Energy](https://fourtet.bandcamp.com/album/new-energy)
      - American Pleasure Club's [A  Whole Fucking Lifetime of This](https://americanpleasureclub.bandcamp.com/album/a-whole-fucking-lifetime-of-this)
      - Field Medic, [Songs from the Sunroom](https://fieldmedic.bandcamp.com/album/songs-from-the-sunroom)
      - Bent Denim, [Town & Country](https://bentdenim.bandcamp.com/album/town-country)
  #+END_SRC
