name: Live Set from Fast-Forward
----
DJ: Lai Power
----
Audio: included
----
Additional:
Recording of a live performance by Lai Power on 16.8.2018 at Fast Forward Festival (www.weareplanc.org/festival/) in the Derbyshire hills. Utopia was reached but sadly only half the set was recorded due to a technical/personal malfunction.

For more music, [Check out Lai Power's Soundcloud](https://soundcloud.com/laipower/)
----
RIYL: Techno, Electronic, Communism
----
SSB:
  name: KawaiiPunk
  key: '@LVL4qjvmws3Cxavfi4iCQI6dSOqWqOyq5/5CHImILA8=.ed25519'
----
License: This set is licensed under a Creative Commons License
