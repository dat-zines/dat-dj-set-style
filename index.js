const choo = require('choo')
const devtools = require('choo-devtools')
const html = require('nanohtml')

const core = require('./javascripts/store/core')
const mainView = require('./javascripts/views/main')

// Initialize choo and bring along it's console helper.
var app = choo()
app.use(devtools())

// Say Hi
console.log('Hello, dear friends!  Thanks for checking out the console!  You are cool.')

// Our Stores
app.use(core)

// Our Routes
app.route('/', mainView)

//Plant that seed.
app.mount('body')
