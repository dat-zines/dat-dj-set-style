const smarkt = require('smarkt')

var zine = new DatArchive(window.location)

module.exports = (state, emitter) => {
  state.infoBoxVisible = false
  state.openingVisible = true

  emitter.on('DOMContentLoaded', () => {
    zine.readFile('distro/info.txt').then(info => {
      state.set = smarkt.parse(info)
      checkForAudio(state.set)
      emitter.emit('render')
    })
  })
  emitter.on('audio included', () => {
    zine.readdir('distro/music').then(contents => {
      state.set.Audio = contents[0]
      emitter.emit('render')
    })
  })

  emitter.on('toggle infoBox', () => {
    state.infoBoxVisible ? state.infoBoxVisible = false : state.infoBoxVisible = true
    emitter.emit('render')
  })

  emitter.on('download requested', (audioFile) => {
    zine.download(`distro/info.txt`)
      .then(success => console.log({success}))
      .catch(err => console.log('error when downloading: ', err))
  })

    function checkForAudio (set) {
      if (!set.Audio) {
        throw new Error('Audio section in distro/info.txt not found.  please add "Audio:" to your info file.  If you are including your music in this zine, you can write Audio: Included.  Otherwise, give the url where we can find your music, e.g. Audio: "dat://coolmusic.cool/track1.mp3"')
      } else {
        var audio = set.Audio.toLowerCase()
        if (audio === 'included') emitter.emit('audio included')
      }
    }
}
