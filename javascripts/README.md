# J A V A S C R I P T S

This holds the various parts of the zine, the skeletal structure of it all.  Every bit of html is actually formatted here.  We do this using [choo](https://choo.io).

I tried to make this zine so you wouldn't have to mess with any of theskeletal stuff and still be able to have something that felt uniquely yours. But this is also owned entirely by you, and you can change every bit of code you want with it.  So if you want to change the skeletal structure, it's here for you!  If you are changing it because my code is not as strong as it could be, please let me know (nicely)!  I barely know what I'm doing and am always trying to learn.   This includes if you know how to make a zine that takes in text files like I have here, but doesn't need to use any JS framework at all.  That'd be cool as hell to know. 

BUT!  For editing these, you'll want to make a local copy of this zine and then make sure you have Node and npm installed on your computer.  You can find out more about both here: [npm](https://docs.npmjs.com/getting-started/what-is-npm) | [node](https://nodejs.org/en/)

Once you have both on your computer, open up your terminal and navigate to this folder, and then install all our npm dependencies:
```
cd dat-dj-set-style
npm install
```

To track your changes as you make them, you'll run the following within the directory:
```
npm run watch
```

Then, open up your site on beaker in preview mode.  Any change made will update here.

When yr happy with your changes, you'll run (still in the directory):
```
npm run build
```

This bundles up all the changes into the new definitive version of yr zine.  Then, you'll want to go to beaker and publish these changes as well.
