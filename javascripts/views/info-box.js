const html = require('choo/html')
const md = require('markdown-it')()
const raw = require('nanohtml/raw')

module.exports = (state, emit) => {
  console.log(state.infoBoxVisible)
  if (state.set && state.infoBoxVisible) {
    var set = state.set
    return html`
     <div id='infoBox'>
       <h2 id='set-name'>'${set.name}'</h2>
       <p id='byline'>by ${set.DJ}</p>
       ${renderAdditionalInfo()}
       <h3 id='SSB'>Find them on SSB:</h3>
       <p> ${set.SSB.name} <code id='ssb-key' class='wormed'><i title=${set.SSB.key}>${set.SSB.key}</i></code></p>
       <p class='instruction'>hover over the worm to see their key, highlight entire worm and press cmd+c to copy to your clipboard.</p>
     </div>
 `
  }
  function renderAdditionalInfo () {
    if (state.set.Additional) {
      var additional = raw(md.render(state.set.Additional))
      return html`
      <div id='additionalInfo'>
        ${additional}
      </div>
    `
    }
  }
}
