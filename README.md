# Welcome to the 'DAT DJ SET STYLE' Zine Template

You are visiting an (incredible) zine that's been made using the dat dj set style template.  This means that you can copy this template and use it as the start of your own (likely also incredible** zine.

## Is this You? Do you wanna make yr own zine kinda like this one?

SWEET! This readme will guide you through how to do this.  But here's a super short version:
- You'll make a copy of this code with a literal click of the button.
- You can  quickly make it yr own by switching out an mp3 and adjusting a text file.
- On top of this, you can adjust the colors and CSS to make it more your own.
- You can even just change all the code to make it fully your own.  It's all yours (as long as you share it just as freely after**.

There's more details on how to do each thing within the folders you see up above.

**The First Folder you wanna check out is Distro***
After that, check out Aesthetic.
Finally, check out javascripts.

# How to Edit and Customize the Zine

The majority of changes can be done entirely within beaker using this here files page (though you can use your local copy and yr favorite text editor too).  Up above is all the folders and files that make up the zine.  click on any file to see its contents.  Then, click the edit pencil in the top right of the text window to edit it as you please.

If you want to adjust the javascript, it's made using [node](https://nodejs.org/en/) and [choo](https://choo.io).  Choo has a great tutorial on their page for getting started with this kinda development.

# Thank Yous
- Thanks to Beaker Browser and its team for this wonderful place.
- Thanks to Kawaiipunk and [SSB](https://ssb.nz), for sharing the [original set](https://soundcloud.com/laipower/live-set-fast-forward-16-8-18) that inspired this here thing.
- Thanks to [Raphael Bastide](https://raphaelbastide.com/) for the delightful code with the ssb hash worms.
- Thanks to choo for the cute and accessible framework for building this.
- Thanks to [smarkt](https://github.com/jondashkyle/smarkt), by jon-kyle, for giving a great way on building out the text files.
- Thanks to Font Awesome for the icons used for download, play, pause, and info.  All icons used under the Creative Commons License, which can be read [here](https://fontawesome.com/license)
- Thanks to the Scuttlebutt commuity for making cool shit all the time.
- Thanks to you for reading this, and making all of this matter!

# Soundtrack
During creation I listened to:
  - a lot of Whitney Ballen's album, [You're a Shooting Star, I'm A Sinking Ship](https://whitneyballen.bandcamp.com/)
  - the lai power set linked above.
  - Four Tet's [New Energy](https://fourtet.bandcamp.com/album/new-energy)
  - American Pleasure Club's [A  Whole Fucking Lifetime of This](https://americanpleasureclub.bandcamp.com/album/a-whole-fucking-lifetime-of-this)
  - Field Medic, [Songs from the Sunroom](https://fieldmedic.bandcamp.com/album/songs-from-the-sunroom)
  - Bent Denim, [Town & Country](https://bentdenim.bandcamp.com/album/town-country)

## Made on a couch and at a kitchen table while...

Spring starts up here in New Zealand, moving back with some friends on their beautiful airship home up in Hataitai, an empty kitchen table and a beautiful view of laundry whipping in the wind on the porch outside and the beautiful ocean just beyond. Taking breaks to eat Angelica's oat bars and go through a kundalini kriya with her.
